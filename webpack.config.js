var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/build/')
    // relative to your project's document root dir
    .setPublicPath('/build')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // will output as web/build/app.js
    .addEntry('home', './web/bundles/app/js/home.js')

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning()

    .enableVueLoader()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();
