install:
	composer install
        php bin/console assets:install --symlink
	yarn run encore dev

start:
	php bin/console server:start

stop:
	php bin/console server:stop

