import Vue from 'vue';
import Search from './Search';

var home = new Vue({
	el: "#app",
	template: '<Search/>',
	components: { Search }
})
